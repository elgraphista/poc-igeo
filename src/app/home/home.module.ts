import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import {QrScannerComponent} from '../modals/qr-scanner/qr-scanner.component';
import {ZXingScannerModule} from '@zxing/ngx-scanner';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage
            }
        ]),
        ZXingScannerModule,
        ReactiveFormsModule
    ],
  declarations: [HomePage, QrScannerComponent],
  entryComponents: [QrScannerComponent]
})
export class HomePageModule {}
