import { Component } from '@angular/core';
import {ModalController, ToastController} from '@ionic/angular';
import {QrScannerComponent} from '../modals/qr-scanner/qr-scanner.component';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  lectura;
  formulario: FormGroup;

  constructor(private modalCtrl: ModalController, private toastCtrl: ToastController) {
    this.formulario = new FormGroup({
      nombre: new FormControl({value: 'Herman Vega', disabled: 'disabled'} ),
      sala: new FormControl({value: '20B', disabled: 'disabled'}),
      direccion: new FormControl({value: 'Blanco Encalada 2070', disabled: 'disabled'}),
      observacion: new FormControl('')
    });
  }

  async leerQR() {
    const modal = await this.modalCtrl.create({
      component: QrScannerComponent
    });
    await modal.present();

    const { data } = await modal.onDidDismiss();
    console.log(data);
    if (data) {
      this.lectura = true;
      const rsp = JSON.parse(atob(data));
      console.log(rsp);
      this.formulario.controls.nombre.setValue(rsp.nombre);
      this.formulario.controls.sala.setValue(rsp.sala);
      this.formulario.controls.direccion.setValue(rsp.direccion);
    }
    // this.lectura = data ? JSON.parse(atob(data)) : 'No se pudo leer';
  }

  sendForm() {
    this.lectura = false;
    this.formulario.reset();
    this.showToast('Formulario enviado...', 3000);
  }

 async showToast(message, duration) {
   const toast = await this.toastCtrl.create({
     message,
     position: 'top',
     duration
   });
   toast.present();
  }

}
