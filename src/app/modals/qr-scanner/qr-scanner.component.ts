import {Component, OnInit, ViewChild} from '@angular/core';
import {BarcodeFormat} from '@zxing/library';
import {ZXingScannerComponent} from '@zxing/ngx-scanner';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-qr-scanner',
  templateUrl: './qr-scanner.component.html',
  styleUrls: ['./qr-scanner.component.scss'],
})
export class QrScannerComponent implements OnInit {
// @ts-ignore
  @ViewChild('scanner') scanner: ZXingScannerComponent;

  allowedFormats = [BarcodeFormat.QR_CODE];
  camaras;

  constructor(public modal: ModalController) { }

  ngOnInit() {}

  scanError(e) {
    console.log('error:', e);
  }

 async scanSuccess(e) {
    console.log('success:', e);
    if (e) {
      await this.modal.dismiss(e).finally();
    }
  }

  scanComplete(e) {
    if (e) {
      console.log('complete:', e);
    }
  }

  camerasFoundHandler(e) {
    this.camaras = e;
  }

  async dvChange(e) {
    console.log('change: ', e);
    const back = await this.camaras.findIndex(asd => asd.label.includes('front'));
    console.log('back: ', back);
    if (back >= 0) {
      delete this.camaras[back];
      this.scanner.device = this.camaras[0];
    } else {
      this.scanner.device = this.camaras[0];
    }
  }

}
